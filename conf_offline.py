# Import analysis/plotting modules
import analysis.event
import analysis.hitfinding
import plotting.image
import plotting.line
import plotting.correlation
import plotting.histogram
from backend.record import add_record
import numpy as np
import time
import ipc

# Quick config parameters
hitScoreThreshold = 9000
aduThreshold = 200

# Specify the facility
state = {}
state['Facility'] = 'BAUHAUS'
# Specify folders with frms6 and darkcal data
state['BAUHAUS/Data1Glob'] = 'mte1/201403*/*/*.png'
#state['BAUHAUS/Data2Glob'] = 'mte2/*/*.png'
state['BAUHAUS/MotorFolder'] = 'motors/stage-server/'
state['do_offline'] = True
state['BAUHAUS/ProcessingRate'] = 100

# This function is called for every single event
# following the given recipe of analysis
def onEvent(evt):
    # Processing rate [Hz]
    analysis.event.printProcessingRate()

    # Do basic hitfinding using lit pixels
    analysis.hitfinding.countLitPixels(evt, evt["photonPixelDetectors"]["MTE1"], 
                                       aduThreshold=aduThreshold, 
                                       hitscoreThreshold=hitScoreThreshold)

    hit = evt["analysis"]["litpixel: isHit"].data
    plotting.line.plotHistory(evt["analysis"]["litpixel: hitscore"],
                              label='Nr. of lit pixels', hline=100, group='Metric')
    analysis.hitfinding.hitrate(evt, hit, history=5000)

    plotting.histogram.plotHistogram(evt["analysis"]["litpixel: hitscore"],
                                     hmin=30000, hmax=45000, bins=50,
                                     name="Lit Pixel Histogram", group="Histograms")

    # Visualize image
    #print evt['photonPixelDetectors']['MTE1'].data.flags
    plotting.image.plotImage(evt['photonPixelDetectors']['MTE1'], group='Images')
    
