# --------------------------------------------------------------------------------------
# Copyright 2016, Benedikt J. Daurer, Filipe R.N.C. Maia, Max F. Hantke, Carl Nettelblad
# Hummingbird is distributed under the terms of the Simplified BSD License.
# Modified 2017 by Kartik Ayyer
# -------------------------------------------------------------------------
"""Creates Hummingbird events for testing purposes"""
import time
import random
from backend.event_translator import EventTranslator
from backend.record import add_record
from backend import Worker
from . import ureg
import numpy as np
import ipc
import glob
import sys
import os
from PIL import Image
import png
import itertools
from datetime import datetime

class BAUHAUSTranslator(object):
    """Creates Hummingbird events for testing purposes"""
    def __init__(self, state):
        self.library = 'BAUHAUS'
        self.state = state
        self.keys = set()
        self.keys.add('analysis')
        self._last_event_time = -1
        self.current_fnames = None
        self.current_size = None
        self.fnum = None
        self.header_dict = None
        if 'do_offline' in state:
            self.do_offline = state['do_offline']
        else:
            self.do_offline = False
        if 'verbose' in state:
            self.verbose = state['verbose']
        else:
            self.verbose = False
        self.folders = self.state['BAUHAUS/DataFolder']
        self.darks = [None] * len(self.folders)
        self.is_dark = [False] * len(self.folders)
        if self.do_offline and ipc.mpi.slave_rank() == 0:
            print 'Running offline i.e. on all files in glob'

    def next_event(self):
        """Generates and returns the next event"""
        evt = {}        
        
        # Check if we need to sleep
        if('BAUHAUS/ProcessingRate' in self.state and self._last_event_time > 0):
            rep_rate = self.state['BAUHAUS/ProcessingRate']
            if('Dummy' in self.state and 'Repetition Rate' in self.state['Dummy']):
                rep_rate = self.state['Dummy']['Repetition Rate'] / float(ipc.mpi.nr_workers())
            target_t = self._last_event_time+1.0/rep_rate
            t = time.time()
            if(t < target_t):
                time.sleep(target_t - t)
        self._last_event_time = time.time()
        
        if self.new_file_check(force=True):
            for i, f in enumerate(self.frames):
                evt['MTE%d'%(i+1)] = f
                evt['header%d'%(i+1)] = Image.open(self.latest_fnames[i]).info
                evt['is_dark%d'%(i+1)] = self.is_dark[i]
            self.keys.add('photonPixelDetectors')
        else:
            if ipc.mpi.slave_rank() == 0:
                sys.stderr.write('Waiting for file list to update\n')
            while True:
                while not self.new_file_check(force=True):
                    time.sleep(0.1)
                
                for i, f in enumerate(self.frames):
                    evt['MTE%d'%(i+1)] = f
                    evt['header%d'%(i+1)] = Image.open(self.latest_fnames[i]).info
                    evt['is_dark%d'%(i+1)] = self.is_dark[i]
                self.keys.add('photonPixelDetectors')
                break
        
        return EventTranslator(evt, self)

    def event_keys(self, _):
        """Returns the translated keys available"""
        return list(self.keys)

    def event_native_keys(self, evt):
        """Returns the native keys available"""
        return evt.keys()

    def translate(self, evt, key):
        """Returns a dict of Records that match a given Hummingbird key"""
        values = {}
        if key == 'photonPixelDetectors':
            # Translate pnCCD
            for i in range(len(self.latest_fnames)):
                add_record(values, key, 'MTE%d'%(i+1), evt['MTE%d'%(i+1)], ureg.ADU)
        elif key == 'motorPositions':
            for motorname,motorpos in evt['header1'].iteritems():
                add_record(values, key, motorname, motorpos, ureg.mm)
        elif key == 'ID':
            add_record(values, key, 'Sample', evt['header1']['Sample'])
            add_record(values, key, 'Pulse ID', evt['header1']['Pulse ID'])
            add_record(values, key, 'Image number', evt['header1']['Image number'])
            for i in range(len(self.latest_fnames)):
                add_record(values, key, 'MTE%d_dark'%(i+1), evt['is_dark%d'%(i+1)])
        elif not key == 'analysis':
            raise RuntimeError('%s not found in event' % key)
        
        return values

    def event_id(self, _):
        """Returns an id which should be unique for each
        shot and increase monotonically"""
        return float(time.time())
        #return self.header_dict['Pulse ID']

    def event_id2(self, _):
        """Returns an alternative id, which is jsut a copy of the usual id here"""
        return event_id

    def new_file_check(self, force=False):
        if self.do_offline:
            '''First time, generate self.flists'''
            if self.fnum is None:
                self.fnum = ipc.mpi.slave_rank()
                self.minute_folders = [glob.glob(f+'/*') for f in self.folders]
                self.flists = [glob.glob(f+'/*/*.png') for f in self.folders]
                for f, m in zip(self.flists, self.folders):
                    print len(f), 'in', m
            else:
                if force and self.fnum < len(self.flists[0]) - 1:
                    self.fnum += ipc.mpi.nr_workers()
            self.latest_fnames = [flist[self.fnum] for flist in self.flists]
        else:
            ''' If online, refresh self.flists every time'''
            self.minute_folders = [glob.glob(f+'/*') for f in self.folders]
            self.latest_minute = [max(f) for f in self.minute_folders]
            self.flists = [glob.glob(f+'/*.png') for f in self.latest_minute]
            self.latest_fnames = [max(flist) for flist in self.flists]
        file_size = os.path.getsize(self.latest_fnames[0])
        if file_size < 1024:
            return False

        if self.latest_fnames != self.current_fnames:
            self.current_size = file_size
            self.current_fnames = self.latest_fnames
            self.check_if_dark()
            if self.verbose and ipc.mpi.slave_rank() == 0:
                print self.latest_fnames
            self.frames = [self.read_frame(f)-d if d is not None else self.read_frame(f) for f,d in zip(self.latest_fnames, self.darks)]
            return True
        elif file_size != self.current_size:
            # Check if the same file has been updated since the last check
            if self.verbose and ipc.mpi.slave_rank() == 0:
                print 'File has changed with new size =', file_size
            self.current_size = file_size
            return True
        else:
            return False

    def read_frame(self, fname):
        return np.vstack(itertools.imap(np.uint16, png.Reader(fname).asDirect()[2])).astype('i4')

    def check_if_dark(self):
        for i, f in enumerate(self.latest_fnames):
            if 'D' in os.path.basename(f):
                print 'Updating dark[%d] with %s' % (i, f)
                self.darks[i] = self.read_frame(f)
                self.is_dark[i] = True
            else:
                self.is_dark[i] = False

    def get_event_time(self, fname):
        return time.mktime(datetime.strptime(os.path.basename(fname)[8:].split('.')[0], '%Y%m%d_%H%M%S').timetuple())
